## Network Topology Visualization

This demo project is to monitor the rpc connections in the node syncing. My purpose is to visualize the network connections between validators in each of the shards. Current, each shard contains 100 to 200 nodes and there are total of 4 shards.

#### Produce network json file

follow the instructions in network-visualize/scripts/[script_name]

1. Download all the validator logs into a folder and parse validator logs using parse_links
1. Run create_network to generate network connection topology of a given shard.
1. (Optional) filter the network according to the inbound/outbound degree of the nodes, it might useful when graph is densely connected

#### Run the http server

Run local http server under network-visualize/ folder

python3 -m http.server

#### Screenshots

Screenshot including all nodes and connections

![sample1](/screenshots/sample1.png?raw=true 'all connections')

Screenshot after mouse over one node. The first number is the ip and second number is the incoming rpc connections

![sample2](/screenshots/sample2.png?raw=true 'click on one node')
