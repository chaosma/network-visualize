var svg = d3.select('svg'),
  width = +svg.attr('width'),
  height = +svg.attr('height');

tooltip = Tooltip('vis-tooltip', 230);

var color = d3.scaleOrdinal(d3.schemeCategory20);

// some parameters to adjust
var get_color = d => color(d.in / 10);
var radius = d => 0.5 * Math.sqrt(d.in);
var stroke_width = 1.0;

var simulation = d3
  .forceSimulation()
  .force(
    'link',
    d3.forceLink().id(function(d) {
      return d.ip;
    }),
  )
  .force('charge', d3.forceManyBody().strength(-150))
  .force('collision', d3.forceCollide().radius(radius))
  .force('center', d3.forceCenter(width / 2, height / 2));
d3.json('data/network_sample.json', function(error, graph) {
  if (error) throw error;
  var link = svg
    .append('g')
    .attr('class', 'links')
    .selectAll('line')
    .data(graph.links)
    .enter()
    .append('line')
    .attr('stroke-width', stroke_width)
    .attr('stroke-opacity', 1.0);
  link.exit().remove();
  var node = svg
    .append('g')
    .attr('class', 'nodes')
    .selectAll('circle')
    .data(graph.nodes)
    //.filter(graph.nodes.in > 20)
    .enter()
    .append('circle')
    .attr('r', radius)
    .attr('fill', get_color)
    .call(
      d3
        .drag()
        .on('start', dragstarted)
        .on('drag', dragged)
        .on('end', dragended),
    )
    .on('mouseover', showDetails)
    .on('mouseout', hideDetails);

  simulation.nodes(graph.nodes).on('tick', ticked);
  simulation.force('link').links(graph.links);
  function ticked() {
    link
      .attr('x1', function(d) {
        return d.source.x;
      })
      .attr('y1', function(d) {
        return d.source.y;
      })
      .attr('x2', function(d) {
        return d.target.x;
      })
      .attr('y2', function(d) {
        return d.target.y;
      });
    node
      .attr('cx', function(d) {
        return d.x;
      })
      .attr('cy', function(d) {
        return d.y;
      });
  }
  function showDetails(d, i) {
    content = '<p class="main">' + d.ip + '</span></p>';
    content += '<hr class="tooltip-hr">';
    content += '<p class="main">' + d.in + '</span></p>';
    tooltip.showTooltip(content, d3.event);
    d3.select(this)
      .style('stroke', 'black')
      .style('stroke-width', 1.0);
    d3.selectAll('line')
      .filter(l => {
        return l.source === d || l.target === d;
      })
      .attr('stroke-width', 1.0 + stroke_width);
    d3.selectAll('line')
      .filter(l => {
        return l.source !== d && l.target !== d;
      })
      .attr('stroke-width', 0.0);
  }
  function hideDetails(d, i) {
    tooltip.hideTooltip();
    node.style('stroke', '#555').style('stroke-width', 0.0);
    d3.selectAll('line').attr('stroke-width', stroke_width);
  }
});

function dragstarted(d) {
  if (!d3.event.active) simulation.alphaTarget(0.3).restart();
  d.fx = d.x;
  d.fy = d.y;
}
function dragged(d) {
  d.fx = d3.event.x;
  d.fy = d3.event.y;
}
function dragended(d) {
  if (!d3.event.active) simulation.alphaTarget(0);
  d.fx = null;
  d.fy = null;
}
